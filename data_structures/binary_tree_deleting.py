"""

    Add following methods to BinarySearchTreeNode class created in main video tutorial

        1. find_min(): finds minimum element in entire binary tree
        2. find_max(): finds maximum element in entire binary tree
        3. calculate_sum(): calcualtes sum of all elements
        4. post_order_traversal(): performs post order traversal of a binary tree
        5. pre_order_traversal(): perofrms pre order traversal of a binary tree
        
"""
#!/usr/bin/env python3.7

from turtle import left


class BinarySearchTreeNode:
    def __init__(self, data) -> None:
        self.data = data
        self.left = None
        self.right = None
        
    def add_child(self, value):
        if self.data == value: 
            return
        
        if value < self.data:
            if self.left:
                self.left.add_child(value)
            else:
                self.left = BinarySearchTreeNode(value)
                
        if value > self.data:
            if self.right:
                self.right.add_child(value)
            else:
                self.right = BinarySearchTreeNode(value)
    
    def search(self, value):
        if self.data == value:
            return True
        
        if value < self.data:
            if self.left:
                return self.left.search(value)
            else:
                return False
        
        if value > self.data:
            if self.right:
                return self.right.search(value)
            else:
                return False
        
    def traverse_in_order(self):
        elements = []
        
        if self.left:
            elements += self.left.traverse_in_order()
            
        elements.append(self.data)
        
        if self.right:
            elements += self.right.traverse_in_order()
            
        return elements      
    
    def traverse_pre_order(self):
        elements = [self.data]
                
        if self.left:
            elements += self.left.traverse_pre_order()
        
        if self.right:
            elements += self.right.traverse_pre_order()
        
        return elements
        
    def traverse_post_order(self):
        elements = []
        
        if self.left: 
            elements += self.left.traverse_post_order()
            
        if self.right:
            elements += self.right.traverse_post_order()
            
        elements.append(self.data)
        
        return elements
    
    def find_min(self):
        # return min(self.traverse_in_order())  
        
        if self.left is None:
            return self.data
        
        return self.left.find_min()
    
    def find_max(self):
        # return max(self.traverse_in_order())
        
        if self.right is None:
            return self.data
        
        return self.right.find_max()
        
    def calculate_sum(self):
        # return sum(self.traverse_in_order())
        
        left_side = self.left.calculate_sum() if self.left else 0
        right_side = self.right.calculate_sum() if self.right else 0
        return self.data + left_side + right_side
    
    def delete(self, value):
        if value < self.data:
            if self.left:
                self.left = self.left.delete(value)
        elif value > self.data:
            if self.right:
                self.right = self.right.delete(value)
        else:
            if self.left is None and self.right is None:
                return None
            if self.left is None: 
                return self.right
            if self.right is None: 
                return self.right
            
            min_val = self.right.find_min()
            self.data = min_val
            self.right = self.right.delete(min_val)
        
        return self


def build_tree(elements):
    root = BinarySearchTreeNode(elements[0])
    
    for i in range(1, len(elements)):
        root.add_child(elements[i])
        
    return root


if __name__=='__main__':
    numbers = [17, 4, 234, 20, 9, 23, 18, 34]
    numbers_tree = build_tree(numbers)
    print(numbers_tree.traverse_in_order())
    print(numbers_tree.search(4))
    print(f"Minimal value in tree is: {numbers_tree.find_min()}")
    print(f"Maximal value in tree is: {numbers_tree.find_max()}")
    print(f"Sum of all elements in binary tree is: {numbers_tree.calculate_sum()}")
    print(numbers_tree.traverse_pre_order())
    numbers_tree.delete(20)
    print(numbers_tree.traverse_in_order())
