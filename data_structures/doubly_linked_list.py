class Node:
    def __init__(self, data=None, next=None, prev=None) -> None:
        self.data = data 
        self.next = next
        self.prev = prev

class LinkedList:
    def __init__(self) -> None:
        self.head = None
        
    def insert_at_beginning(self, data): 
        node = Node(data, self.head, None)
        
        if self.head == None:
            self.head = node
        else:
            self.head.prev = node
            self.head = node
            
    def insert_at_end(self, data):        
        if self.head == None:
            self.head = Node(data, None, None)
            return
        
        itr = self.head
        
        while itr.next:
            itr = itr.next
            
        itr.next = Node(data, None, itr)
        
    def insert_at(self, index, data):
        if index > self.get_list_length() - 1 or index < 0:
            raise Exception("Invalid index")
        
        if index == 0:
            self.insert_at_beginning(data)
            return
        
        itr = self.head
        
        for _ in range(index - 1):
            itr = itr.next
        
        node = Node(data, itr.next, itr)
        
        # if inserted elemented will be last one
        if node.next:
            node.next.prev = node
        itr.next = node
        
    def insert_after_value(self, data_after, data_to_insert):
        if self.head == None:
            return
        
        itr = self.head
        
        while itr:
            if itr.data == data_after:
                node = Node(data_to_insert, itr.next, itr)
                itr.next = node
                if node.next:
                    node.next.prev = node
                return
            itr = itr.next   
        
        raise Exception("Invalid value")
            
    def add_values(self, data_list):
        self.head = None
        
        for data in data_list:
            self.insert_at_end(data)
            
    def remove_at(self, index):
        if index > self.get_list_length() - 1 or index < 0:
            raise Exception("Invalid index")
        
        if index == 0:
            self.head = self.head.next
            self.head.prev = None
            return
        
        itr = self.head
        
        for _ in range(index - 1):
            itr = itr.next
            
        if itr.next.next:
            itr.next = itr.next.next     
            itr.next.prev = itr
            
        itr.next = None 
    
    def remove_by_value(self, data):
        if self.head == None:
            return
        
        if self.head.data == data:
            self.head = self.head.next
            self.head.prev = None
            return
            
        itr = self.head
        
        while itr: 
            try:
                if itr.next.data == data:
                    if itr.next.next:
                        itr.next = itr.next.next
                        itr.next.prev = itr
                        return
                    itr.next = None
                    return
            except AttributeError:
                print("Invalid data")
            
            itr = itr.next
            
        raise Exception("Invalid value")
    
    def get_list_length(self):
        amount = 0
        
        itr = self.head
        
        while itr:
            itr = itr.next
            amount += 1 
            
        return amount
    
    def get_last_element(self):
        itr = self.head
        while itr.next:
            itr = itr.next
            
        return itr
        
    def print_forward(self) -> None:
        if self.head == None:
            print("List is empty!")
            return
        
        itr = self.head
        string_list = ''
        while itr:
            string_list += f"{itr.data} --> "
            itr = itr.next
            
        print(string_list)
    
    def print_backward(self) -> None:
        if self.head == None:
            print("List is empty!")
            return
        
        itr = self.get_last_element()
        string_list = ''
        
        while itr:
            string_list += f"{itr.data} --> "
            itr = itr.prev
            
        print(string_list)
            

if __name__ == "__main__":
    ll = LinkedList()         
    ll.insert_at_beginning(5)
    ll.insert_at_beginning(8)
    ll.insert_at_end(10)
    ll.print_forward()
    ll.insert_at(0, 100)
    ll.print_forward()
    ll.insert_after_value(10, 9999)
    print("FORWARD:")
    ll.print_forward()
    print("BACKWARD")
    ll.print_backward()
    print()
    ll.remove_at(3)
    ll.print_forward()
    ll.remove_by_value(5)
    ll.print_forward()
    print(ll.get_list_length())
    print(ll.get_last_element().data)
