from shutil import ExecError


class HashTable:
    def __init__(self) -> None:
        self.MAX = 10
        self.arr = [None for _ in range(self.MAX)]
        
    def get_hash(self, key):
        hash = 0 
        for char in key:
            hash += ord(char)
        return hash % self.MAX
    
    def __setitem__(self, key, value):
        hash_index = self.get_hash(key)
        
        if self.arr[hash_index] == None:
            self.arr[hash_index] = (key, value)
        else:
            new_hash_index = self.find_free_slot(key, hash_index)
            self.arr[new_hash_index] = (key, value)
        print(self.arr)
        
    def __getitem__(self, key):
        hash = self.get_hash(key)
        if self.arr[hash] == None:
            raise Exception("There is not such element in hashmap")
        prob_range = self.get_prob_range(hash)
        for prob_index in prob_range:
            element = self.arr[prob_index]
            if element == None:
                raise Exception("There is not such element in hashmap")
            if element[0] == key:
                return element[1]
        
    def __delitem__(self, key):
        hash = self.get_hash(key)
        
        if self.arr[hash] == None:
            raise Exception("There is not such element in hashmap")
        
        prob_range = self.get_prob_range(hash)
        for prob_index in prob_range:
            element = self.arr[prob_index]
            if element == None:
                raise Exception("There is not such element in hashmap")
            if element[0] == key:
                self.arr[prob_index] = None
            
        
    def find_free_slot(self, key, hash_index):
        prob_range = self.get_prob_range(hash_index)
        for prob_index in prob_range:
            if self.arr[prob_index] == None:
                return prob_index
            if self.arr[prob_index][0] == key:
                return prob_index
        raise Exception("Hashmap full")
    
    def get_prob_range(self, hash_index):
        return [*range(hash_index, len(self.arr))] + [*range(0, hash_index)]

if __name__ == "__main__":
    ht = HashTable()
    ht['march 6'] = 310
    ht['march 7'] = 340
    ht['march 8'] = 380
    ht['march 17'] = 310
    ht['march 18'] = 33
    
    # print(ht["march 10"])
    print(ht["march 6"])
    print(ht["march 7"])
    print(ht["march 8"])
    print(ht["march 17"])
    print(ht["march 18"])

    print(ht.arr)

    
    