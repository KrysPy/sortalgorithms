from collections import deque

class Stack:
    def __init__(self) -> None:
        self.container = deque()
        
    def push(self, value):
        self.container.append(value)
        
    def pop(self):
        return self.container.pop()
    
    def peek(self):
        """
            Return last element but not remove it from stack
        """
        return self.container[-1]
    
    def is_empty(self):
        return len(self.container) == 0
    
    def size(self):
        return len(self.container)
    

if __name__ == "__main__":
    stack = Stack()
    stack.push(10)
    print(stack.peek())
    print(stack.pop())
    print(stack)
    print(stack.is_empty())