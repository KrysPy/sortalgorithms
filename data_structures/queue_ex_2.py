"""
Write a program to print binary numbers from 1 to 10 using Queue. Use Queue class implemented in main tutorial. Binary sequence should look like,
    1
    10
    11
    100
    101
    110
    111
    1000
    1001
    1010
    
Hint: Notice a pattern above. After 1 second and third number is 1+0 and 1+1. 4th and 5th number are second number (i.e. 10) + 0 and second number (i.e. 10) + 1.

You also need to add front() function in queue class that can return the front element in the queue.
"""

import queue

class BinaryNumbers:
    def __init__(self) -> None:
        self.binary = queue.Queue()
        self.binary.enqueue('1')

        
    def generate_binaries_list(self, binaries_amount: int):
        if binaries_amount == 0:
            return
        
        for i in range(binaries_amount):
            front_num = self.binary.get_front_val()
            print("  ", front_num)
            self.binary.enqueue(front_num + "0")
            self.binary.enqueue(front_num + "1")
        
            self.binary.dequeue()


if __name__ == '__main__':
    bn = BinaryNumbers()
    bn.generate_binaries_list(10)