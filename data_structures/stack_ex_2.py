"""
    
    Write a function in python that checks if paranthesis in the string are balanced or not. Possible parantheses are "{}',"()" or "[]". Use Stack class from the tutorial.
is_balanced("({a+b})")     --> True
is_balanced("))((a+b}{")   --> False
is_balanced("((a+b))")     --> True
is_balanced("))")          --> False
is_balanced("[a+b]*(x+2y)*{gg+kk}") --> True

"""
    
from collections import deque
    
class Stack:
    def __init__(self) -> None:
        self.container = deque()
        
    def push(self, value):
        self.container.append(value)
        
    def peek(self):
        return self.container[-1]
    
    def pop(self):
        return self.container.pop()
    
    def is_empty(self):
        return len(self.container) == 0
    
    def size(self):
        return len(self.container)
    
    
def is_match(char_1, char_2):
    match_dict = {
        ')': '(',
        ']': '[',
        '}': '{'
    }
    return match_dict[char_1] == char_2
    
    
def is_balanced(string: str):
    stack = Stack()
    for char in string:
        if char in ['(', '[', '{']:
            stack.push(char)
        if char in [')', ']', '}']:
            if stack.size() == 0:
                return False
            if not is_match(char, stack.pop()):
                return False
            
    return stack.size() == 0
    
    
print(is_balanced("({a+b})"))
print(is_balanced("))((a+b}{"))
print(is_balanced("((a+b))"))
print(is_balanced("((a+g))"))
print(is_balanced("))"))
print(is_balanced("[a+b]*(x+2y)*{gg+kk}"))
