import csv
import hash_table_collision_handling as hash_table

# nyc_weather.csv contains new york city weather for first few days in the month of January. Write a program that can answer following,
# What was the temperature on Jan 9?
# What was the temperature on Jan 4?

class Weather:
    def __init__(self) -> None:
        self.ht_weather = hash_table.HashTable()

    def load_data(self, file_name: str):
        with open(file_name, 'r') as csv_f:
            csv_reader = csv.reader(csv_f)
            next(csv_reader)
            for line in csv_reader:
                self.ht_weather[line[0]] = line[1]
        
    def get_dat_temprature(self, day: str):
        return self.ht_weather[day]


if __name__ == "__main__":
    w = Weather()
    w.load_data("nyc_weather.csv") 
    print(w.get_dat_temprature("Jan 9"))
    print(w.get_dat_temprature("Jan 4"))

       
    