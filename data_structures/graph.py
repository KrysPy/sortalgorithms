from tkinter import E


class Graph:
    def __init__(self, edges) -> None:
        self.edges = edges
        self.graph_dict = {}
        for start, end in edges:
            if start in self.graph_dict.keys():
                if end in self.graph_dict[start]:
                    pass
                else:
                    self.graph_dict[start].append(end)
            else:
                self.graph_dict[start] = [end]
                
    def get_paths(self, start, end, path=[]):
        path = path + [start]
        
        if start == end: return [path]
        
        if start not in self.graph_dict.keys(): return []
        
        paths = []
        for node in self.graph_dict[start]:
            if node not in path:
                new_path = self.get_paths(node, end, path)
                for path in new_path:
                    paths.append(path)
        return paths
    
    def get_shortest_path(self, start, end, path=[]):
        path = path + [start]
        
        if start not in self.graph_dict.keys(): return None
        
        if start == end: return path
        
        shortest_path = None
        for node in self.graph_dict[start]:
            if node not in path:
                sp = self.get_shortest_path(node, end, path)
                if sp:
                    if shortest_path is None or len(sp) < len(shortest_path):
                        shortest_path = sp
        return shortest_path


if __name__ == '__main__':
    routes = [
        ("Mumbai", "Paris"),
        ("Mumbai", "Dubai"),
        ("Paris", "Dubai"),
        ("Paris", "New York"),
        ("Dubai", "New York"),
        ("New York", "Toronto"),
    ]
    
    route_graph = Graph(routes)
    start = "Mumbai"
    end = "New York"
    
    print(f"Paths between {start} and {end}: {route_graph.get_shortest_path(start, end)}")
