class Node:
    def __init__(self, data=None, next=None) -> None:
        self.data = data 
        self.next = next

class LinkedList:
    def __init__(self) -> None:
        self.head = None
        
    def insert_at_beginning(self, data): 
        node = Node(data, self.head)
        self.head = node
        
    def insert_at_end(self, data):
        node = Node(data, None)
        
        if self.head is None:
            self.head = Node(data, None)
            return
        
        itr = self.head
        
        while itr.next:
            itr = itr.next
            
        itr.next = node
        
    def insert_at(self, index, data):
        if index > self.get_list_length() - 1 or index < 0:
            raise Exception("Invalid index")
        
        if index == 0:
            self.insert_at_beginning(data)
            return
        
        itr = self.head
        
        for _ in range(index - 1):
            itr = itr.next
            
        itr.next = Node(data, itr.next)
        
    def insert_after_value(self, data_after, data_to_insert):
        if self.head is None:
            return
        
        itr = self.head
        
        while itr:
            if itr.data == data_after:
                itr.next = Node(data_to_insert, itr.next)
                return
            itr = itr.next   
        
        raise Exception("Invalid value")
            
    def add_values(self, data_list):
        self.head = None
        
        for data in data_list:
            self.insert_at_end(data)
            
    def remove_at(self, index):
        if index > self.get_list_length() - 1 or index < 0:
            raise Exception("Invalid index")
        
        if index == 0:
            self.head = self.head.next
            return
        
        itr = self.head
        
        for _ in range(index - 1):
            itr = itr.next

        itr.next = itr.next.next         
    
    def remove_by_value(self, data):
        if self.head is None:
            return
        
        if self.head.data == data:
            self.head = self.head.next
            return
            
        itr = self.head
        
        while itr: 
            try:
                if itr.next.data == data:
                    itr.next = itr.next.next
                    return
            except AttributeError:
                print("Invalid data")
            
            itr = itr.next
            
        raise Exception("Invalid value")
    
    def get_list_length(self):
        amount = 0
        
        itr = self.head
        
        while itr:
            itr = itr.next
            amount += 1 
            
        return amount
    
    def print(self):
        if self.head == None:
            print("Linked list is empty")
            return
        
        itr = self.head
        llstr = ''
        while itr:
            llstr += str(itr.data) + ' --> '
            itr = itr.next
        
        print(llstr)
    

if __name__ == "__main__":
    ll = LinkedList()         
    ll.insert_at_beginning(5)
    ll.insert_at_end(99)
    ll.insert_at_beginning(7)
    ll.insert_at_beginning(8)
    ll.insert_at_end(100)
    ll.print()
    print(ll.get_list_length())
    # ll.remove_at(3)
    ll.insert_at(2, 999)
    ll.insert_at_end("new")
    ll.print()
    ll.get_list_length()
    ll.insert_after_value(8, 66666)
    ll.print()
    ll.remove_by_value(8)
    ll.print()