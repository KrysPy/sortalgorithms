"""
    Write a function in python that can reverse a string using stack data structure. Use Stack class from the tutorial.
    reverse_string("We will conquere COVID-19") should return "91-DIVOC ereuqnoc lliw eW"
"""

from collections import deque

class Stack: 
    def __init__(self) -> None:
        self.container = deque()
        
    def push(self, value):
        self.container.append(value)
        
    def pop(self):
        return self.container.pop()
    
    def peek(self):
        return self.container[-1]
    
    def is_empty(self):
        return len(self.container) == 0
    
    def size(self):
        return len(self.container)
    
    def reverse_string(self, string: str):
        for char in string:
            self.push(char)
            
        reversed_str = ''
        for _ in range(self.size()):
            reversed_str += self.pop()
            
        return reversed_str
            
if __name__ == "__main__":
    stack = Stack()
    print(stack.reverse_string("We will conquere COVID-19"))
