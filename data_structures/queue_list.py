# list queue implementation 

wmt_stock_price_queue = []

wmt_stock_price_queue.insert(0, 131.10)
wmt_stock_price_queue.insert(0, 132.12)
wmt_stock_price_queue.insert(0, 135)

print(wmt_stock_price_queue)

print(wmt_stock_price_queue.pop())

print(wmt_stock_price_queue)
print()

# ----------------------------------------------------------------------------
# collection.deque implementation
from collections import deque
d = deque()

d.appendleft(5)
d.appendleft(8)
d.appendleft(27)

print(d.pop())
print()
# ----------------------------------------------------------------------------
# PYTHON QUEUE IMPLEMENTATION 
class Queue:
    def __init__(self) -> None:
        self.buffer = deque()
        
    def enqueue(self, value):
        self.buffer.appendleft(value)
        
    def dequeue(self):
        return self.buffer.pop()
    
    def is_empty(self):
        return len(self.buffer) == 0
    
    def size(self):
        return len(self.buffer)
    
if __name__ == '__main__':
    pq = Queue()

    pq.enqueue({
        'company': 'Wall Mart',
        'timestamp': '15 apr, 11.01 AM',
        'price': 131.10
    })
    pq.enqueue({
        'company': 'Wall Mart',
        'timestamp': '15 apr, 11.02 AM',
        'price': 132
    })
    pq.enqueue({
        'company': 'Wall Mart',
        'timestamp': '15 apr, 11.03 AM',
        'price': 135
    })
    
    print(pq.dequeue())
    print(pq.size())