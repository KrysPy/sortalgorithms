"""
   https://github.com/codebasics/data-structures-algorithms-python/blob/master/data_structures/7_Tree/7_tree_exercise.md 
"""

class TreeNode:
   def __init__(self, name, designation) -> None:
       self.name = name
       self.designation = designation 
       self.children = []
       self.parent = None
       
   def add_child(self, child):
      child.parent = self
      self.children.append(child)
      
   def get_level(self):
      level = 0
      p = self.parent
      while p:
         level += 1
         p = p.parent
      return level
   
   def print_tree(self, mode: str):
      spaces = " " * self.get_level() * 3
      prefix = spaces + "|__ " if self.parent else ""
      
      if mode.lower() == "both":
         print(prefix + self.name + " (" + self.designation + ")")
      elif mode.lower() == "name":
         print(prefix + self.name)
      elif mode.lower() == "designation":
         print(prefix + self.designation)
      else:
         raise Exception("Wrong mode!")

      if self.children:
         for child in self.children:
            child.print_tree(mode)
            

def build_product_tree():
   root = TreeNode("Nilupul", "CEO")
   
   chinmay = TreeNode("Chinmay", "CTO")

   vishwa = TreeNode("Vishwa", "Infrastructure")
   vishwa.add_child(TreeNode("Dhaval", "Cloud Manager"))
   vishwa.add_child(TreeNode("Abijit", "App Manager"))
   
   gels = TreeNode("Gels", "HR Head")
   gels.add_child(TreeNode("Peter", "Recruitment Maganer"))
   gels.add_child(TreeNode("Waqas", "Policy Manager"))
   
   chinmay.add_child(vishwa)
   chinmay.add_child(TreeNode("Aamir", "Application Head"))
   
   root.add_child(chinmay)
   root.add_child(gels)
   
   return root


if __name__ == '__main__':
   root_node = build_product_tree()
   root_node.print_tree("name")
   print()
   root_node.print_tree("designation")
   print()
   root_node.print_tree("both")
   print()
   root_node.print_tree("wrong")
