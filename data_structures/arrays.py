"""
    Dynamic Memory Alocation
    When the space in current located memory is over and for example you have
    busy 10 spaces in memory and you want to add 11th element. So that 10 elements will alocate to new spaces in memory
    and add to this n (spaces) memory: 2 * n(spaces), so in new space will be 30 spaces for data and so on.
    2 * n is called geometric progression.

    When you search by index is O(1) -> it is constant time operation
    Searching by value: O(n) -> you have to do n numbers of iterations to find a number


"""

