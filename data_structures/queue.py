from collections import deque

class Queue:
    def __init__(self) -> None:
        self.buffer = deque()
        
    def enqueue(self, value):
        self.buffer.appendleft(value)
        
    def dequeue(self):
        if self.is_empty():
            raise Exception("Queue is empty")
        
        return self.buffer.pop()
    
    def is_empty(self):
        return len(self.buffer) == 0
    
    def size(self):
        return len(self.buffer)
    
    def get_front_val(self):
        return self.buffer[-1]
    
    