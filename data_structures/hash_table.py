class HashTable:
    def __init__(self) -> None:
        self.MAX = 100
        self.arr = [None for _ in range(self.MAX)]
        
    def get_hash(self, key: str):
        h = 0 
        for char in key:
            h += ord(char)
        return h % self.MAX
    
    def __setitem__(self, key, value):
        h = self.get_hash(key)
        self.arr[h] = value
        
    def __getitem__(self, key):
        h = self.get_hash(key)
        return self.arr[h]
    
    def __delitem__(self, key):
        h = self.get_hash(key)
        self.arr[h] = None
    

if __name__ == "__main__":
    ht = HashTable()
    ht['march 6'] = 130
    print(ht['march 6'])
    ht['march 1'] = 20
    print(ht['march 1'])
    del ht['march 1']
    print(ht['march 1'])
    