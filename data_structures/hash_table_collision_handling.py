# CHAINING COLLSION HANDLING
# 
class HashTable:
    def __init__(self) -> None:
        self.MAX = 10
        self.arr = [[] for _ in range(self.MAX)]
        
    def get_hash(self, key: str):
        h = 0 
        for char in key:
            h += ord(char)
        return h % self.MAX
    
    def __setitem__(self, key, value):
        h = self.get_hash(key)
        found = False
        
        for idx, element in enumerate(self.arr[h]):
            # when we ahve already the same key and we want overwrite this 
            if len(element) == 2 and element[0] == key:
                self.arr[h][idx] = (key, value)
                found = True
                break
        
        if not found:
            self.arr[h].append((key, value))
        
    def __getitem__(self, key):
        h = self.get_hash(key)
        
        for element in self.arr[h]:
            if element[0] == key:
                return element[1]
    
    def __delitem__(self, key):
        h = self.get_hash(key)
        
        for idx, element in enumerate(self.arr[h]):
            if element[0] == key:
                del self.arr[h][idx]
                

if __name__ == "__main__":
    ht = HashTable()
    ht['march 6'] = 130
    # ht['march 6'] = 783
    ht["march 8"] = 67
    ht["amrch 9"] = 4
    ht['march 17'] = 459
    print(ht.arr)
    print(ht['amrch 9'])
    print()
    print(ht['march 6'])
    