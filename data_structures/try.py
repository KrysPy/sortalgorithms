def insertion_sort(lst):
    for i in range(1, len(lst)):
        anchor = lst[i]
        j = i - 1
        while j >= 0 and anchor < lst[j]:
            lst[j + 1] = lst[j]
            j -= 1
        lst[j + 1] = anchor
    return lst


lst = [1, 5, 2, 7, 2, 3, 2, 1]
print(insertion_sort(lst))
