"""
    Modify delete method in class BinarySearchTreeNode class to use min element from left subtree. 
    You will remove lines marked with ---> and use max value from left subtree
"""
# sth
from xml.dom.minidom import Element


class BinarySearchTreeNode:
    def __init__(self, data) -> None:
        self.data = data
        self.left = None
        self.right = None
    
    def add_child(self, value) -> bool:
        if self.data == value:
            return
        
        if value < self.data:
            if self.left:
                self.left.add_child(value)
            else:
                self.left = BinarySearchTreeNode(value)
                
        if value > self.data:
            if self.right:
                self.right.add_child(value)
            else:
                self.right = BinarySearchTreeNode(value)
                
    def search(self, value):
        if self.data == value:
            return True
        
        if value < self.data:
            if self.left:
                self.left.search(value)
            else:
                return False
            
        if value > self.data: 
            if self.right:
                self.right.search(value) 
            else:
                return False
            
    def in_order_traversal(self) -> list:
        elements = []
        
        if self.left:
            elements += self.left.in_traverse_order()
            
        elements.append(self.data)
        
        if self.right:
            elements += self.right.in_traverse_order()
            
        return elements
    
    def pre_order_traversal(self) -> list:
        elements = [self.data]
        
        if self.left:
            elements += self.left.pre_order_traversal()
            
        if self.right:
            elements += self.right.pre_order_traversal()
        
        return elements
    
    def post_order_traversal(self) -> list:
        elements = []
        
        if self.left:
            elements += self.left.post_order_traversal()
        
        if self.right:
            elements += self.right.post_order_traversal()
            
        elements.append(self.data)
        
        return elements
        
    def find_min(self):
        if self.left is None:
            return self.data
        return self.left.find_min()
    
    def find_max(self):
        if self.right is None:
            return self.data
        return self.right.find_max()
    
    def calculate_sum(self):
        left_side = self.left.calculate_sum() if self.left else 0
        right_side = self.right.calculate_sum() if self.right else 0
        return self.data + left_side + right_side

    def delete(self, value):
        if value < self.data:
            if self.left:
                self.left = self.left.delete(value)
        elif value > self.data:
            if self.right:
                self.right = self.right.delete(value)
        else:
            if self.left is None and self.right is None:
                return None
            if self.left is None:
                return self.right
            if self.right is None:
                return self.left
            
            min_val = self.right_find_min()
            self.data = min_val
            self.right = self.right.delete(min_val)
            
        return self
    
    
def build_tree(elements: list) -> BinarySearchTreeNode:
    root = BinarySearchTreeNode(elements[0])
    
    for i in range(1, len(elements)):
        root.add_child(elements[i])
    
    return root


if __name__ == '__main__':
    pass
    