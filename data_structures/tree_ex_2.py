"""
   https://github.com/codebasics/data-structures-algorithms-python/blob/master/data_structures/7_Tree/7_tree_exercise.md 
"""


class TreeNode:
   def __init__(self, data) -> None:
      self.data = data
      self.children = []
      self.parent = None
      
   def add_child(self, child):
      child.parent = self
      self.children.append(child)

   def get_level(self):
      level = 0
      p = self.parent
      while p:
         level += 1
         p = p.parent
      return level
         
   def print_tree(self, tree_levels: int):
      if tree_levels < self.get_level():
         return
      spaces = 3 * " " * self.get_level()
      prefix = spaces + "|__" if self.parent else ""
      print(prefix + self.data)
      
      if self.children:
         for child in self.children:
            child.print_tree(tree_levels)
         

def build_product_tree():
   root = TreeNode("Global")
   
   india = TreeNode("India")
   
   gujarat = TreeNode("Gujarad")
   gujarat.add_child(TreeNode("Ahmedabad"))
   gujarat.add_child(TreeNode("Baroda"))
   
   karnataka = TreeNode("Karnataka")
   karnataka.add_child(TreeNode("Bangluru"))
   karnataka.add_child(TreeNode("Mysore"))
   
   india.add_child(gujarat)
   india.add_child(karnataka)
   root.add_child(india)
   
   usa = TreeNode("USA")
   
   new_jersey = TreeNode("New Jersey")
   new_jersey.add_child(TreeNode("Princeton"))
   new_jersey.add_child(TreeNode("Trenton"))
   
   california = TreeNode("California")
   california.add_child(TreeNode("San Francisco"))
   california.add_child(TreeNode("Mountain View"))
   california.add_child(TreeNode("Palo Alto"))
   
   usa.add_child(new_jersey)
   usa.add_child(california)
   root.add_child(usa)
   
   return root


if __name__ == '__main__':
   root = build_product_tree()
   root.print_tree(1)
   print()
   root.print_tree(2)
   print()
   root.print_tree(3)
