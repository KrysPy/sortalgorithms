"""
    For all exercises use Queue class implemented in main tutorial.

Design a food ordering system where your python program will run two threads,

Place Order: This thread will be placing an order and inserting that into a queue. This thread places new order every 0.5 second. (hint: use time.sleep(0.5) function)
Serve Order: This thread will server the order. All you need to do is pop the order out of the queue and print it. This thread serves an order every 2 seconds. 
Also start this thread 1 second after place order thread is started.
Use this video to get yourself familiar with multithreading in python

Pass following list as an argument to place order thread,

orders = ['pizza','samosa','pasta','biryani','burger']

This problem is a producer,consumer problem where place_order thread is producing orders whereas server_order thread is consuming the food orders. 
Use Queue class implemented in a video tutorial.
"""

import threading
import queue
import time


class FoodOrdering:
    def __init__(self) -> None:
        self.order_buffer = queue.Queue()
        
    def place_order(self, orders: list):
        for order in orders:
            self.order_buffer.enqueue(order)
            print(f"{order} placed")
            time.sleep(0.5)
            
    def serve_order(self):
        time.sleep(1)
        
        while not self.order_buffer.is_empty():
            print(f"Now is serving: {self.order_buffer.dequeue()}")
            time.sleep(2)
    
    
if __name__ == '__main__':
    fo = FoodOrdering()
    orders = ['pizza','samosa','pasta','biryani','burger']


    placing_thread = threading.Thread(target=fo.place_order, args=(orders, ))
    serving_thread = threading.Thread(target=fo.serve_order)

    placing_thread.start()
    serving_thread.start()
    