# nyc_weather.csv contains new york city weather for first few days in the month of January. Write a program that can answer following,
# What was the average temperature in first week of Jan
# What was the maximum temperature in first 10 days of Jan

from ast import Raise
import csv

class Weather:
    def __init__(self) -> None:
        self.all_temps = []
        
    def load_temp(self, file_name: str):
        with open(file_name, 'r') as csv_f:
            csv_reader = csv.reader(csv_f)
            
            next(csv_reader)
        
            self.all_temps = [int(line[1]) for line in csv_reader]
    
    def calc_average_temp(self, days_amount: int):
        if days_amount > len(self.all_temps):
            raise Exception("Invalid value")
            
        return sum(self.all_temps[:days_amount]) / days_amount

    def get_max_temp(self, days_amount: int):
        if days_amount > len(self.all_temps):
            raise Exception("Invalid value")
            
        return max(self.all_temps[:days_amount])

            
if __name__ == "__main__":
    w = Weather()
    w.load_temp("nyc_weather.csv")
    print(w.all_temps)
    print(f"Average temprature in firt week of Jan: {round(w.calc_average_temp(7), 2)}")
    print(f"The maximum temprature in first 10 days of Jan: {w.get_max_temp(10)}")
