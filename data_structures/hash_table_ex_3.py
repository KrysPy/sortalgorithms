import pprint

counted_words = {}

with open("poem.txt", "r") as file:
    for line in file:
        current_line_words = line.split(' ')
        for word in current_line_words:
            word = word.replace('\n', '')
            if word in counted_words:
                counted_words[word] += 1
            else:
                counted_words[word] = 1


pprint.pprint(counted_words)
keys = list(counted_words.keys())
print(keys)
keys.sort(reverse=True)
print(keys)