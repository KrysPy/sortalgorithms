def partition(_list: list, start: int, end: int):
    pivot = _list[end]
    partition_idx = start
    for i in range(start, end):
        if _list[i] <= pivot:
            _list[partition_idx], _list[i] = _list[i], _list[partition_idx]
            partition_idx += 1

    _list[partition_idx], _list[end] = _list[end], _list[partition_idx]

    return partition_idx


def quick_sort(_list: list, start: int, end: int):
    if len(_list) == 0:
        return

    if start < end:
        pivot_idx = partition(_list, start, end)
        quick_sort(_list, start, pivot_idx - 1)
        quick_sort(_list, pivot_idx + 1, end)


if __name__ == '__main__':
    test_cases = [
        [11, 9, 29, 7, 2, 15, 28],
        [3, 7, 9, 11],
        [25, 22, 21, 10],
        [29, 15, 28],
        [],
        [6],
        [2, 1],
        [2, 1, 2]
    ]

    for test_case in test_cases:
        quick_sort(test_case, 0, len(test_case) - 1)
        print(f'Sorted array: {test_case}')

