import pprint
import random


def bubble_sort(lst: list):

    amount_of_elements = len(lst)

    for i in range(0, amount_of_elements):
        for j in range(1, amount_of_elements - i):
            if lst[j] < lst[j - 1]:
                lst[j], lst[j - 1] = lst[j - 1], lst[j]

    return lst


def dict_bubble_sort(_list: list, key='transaction_amount'):
    list_size = len(_list)
    for j in range(list_size - 1):
        for i in range(list_size - 1 - j):
            if _list[i][key] > _list[i + 1][key]:
                _list[i], _list[i + 1] = _list[i + 1], _list[i]


if __name__ == '__main__':
    # Data
    random_list = [random.randint(1, 100) for _ in range(100)]
    _list = ['f', 'd', 's', 'g', 'e', 'a', 'z', 'b', 'd', 'c']
    _list2 = ["mona", "dahal", "amir", "Luke", "Amy"]
    _list3 = [
        {'name': 'mona', 'transaction_amount': 1000, 'device': 'iphone-10'},
        {'name': 'dhaval', 'transaction_amount': 400, 'device': 'google pixel'},
        {'name': 'kathy', 'transaction_amount': 200, 'device': 'vivo'},
        {'name': 'aamir', 'transaction_amount': 800, 'device': 'iphone-8'},
    ]

    # Sorting
    bubble_sort(random_list)
    bubble_sort(_list)
    bubble_sort(_list2)
    dict_bubble_sort(_list3)

    # Outputs
    pprint.pprint(_list3)
    print(_list)
    print(_list2)
    print(f"Random list: {random_list}")
    print(random_list)
