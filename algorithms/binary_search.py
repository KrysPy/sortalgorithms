# binary search work on sorted  arrays
# binary search time complexity is O(log n).
# linear search time complexity is O(n)

new_lst = [12, 15, 17, 19, 21, 21, 24, 45, 67]
x = 21


def binary_search(lst: list, low: int, high: int, x: int):

    # main checking because of recursive
    if high >= low:

        mid = (low + high) // 2

        if lst[mid] == x:
            return mid
        elif lst[mid] > x:
            return binary_search(lst, low, mid - 1, x)
        else:
            return binary_search(lst, mid + 1, high, x)
    else:
        return -1


result = binary_search(new_lst, 0, len(new_lst) - 1, x)

if result != -1:
    print(f"Searching number has {result} index in list: {new_lst}")
else:
    print(f"There is not '{x}' in list.")
