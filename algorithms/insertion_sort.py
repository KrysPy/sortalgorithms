import random


def insertion_sort(_list: list):
    for i in range(1, len(_list)):
        anchor = _list[i]
        j = i - 1
        while j >= 0 and anchor < _list[j]:
            _list[j + 1] = _list[j]
            j -= 1
        _list[j + 1] = anchor


if __name__ == '__main__':
    test_cases = [
        [7, 8, 5, 2, 4, 6, 3],
        [11, 9, 29, 7, 2, 15, 28],
        [3, 7, 9, 11],
        [25, 22, 21, 10],
        [29, 15, 28],
        [],
        [6],
        [2, 1],
        [2, 1, 2]
    ]

    for test_case in test_cases:
        insertion_sort(test_case)
        print(f'Sorted array: {test_case}')
