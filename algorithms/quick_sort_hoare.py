def partition(_list: list, start: int, end: int):
    pivot_idx = start
    pivot = _list[pivot_idx]
    while start < end:
        # len statement because all next elements are smaller than pivot and start will be increase all the  time
        # and the start value will be bigger than len of list and it raise the out of range error
        while start < len(_list) and _list[start] <= pivot:
            start += 1

        while _list[end] > pivot:
            end -= 1

        if start < end:
            _list[start], _list[end] = _list[end], _list[start]

    _list[pivot_idx], _list[end] = _list[end], _list[pivot_idx]

    return end


def quicksort(_list: list, start: int, end: int):
    if len(_list) == 0:
        return _list

    if start < end:
        pivot_idx = partition(_list, start, end)
        quicksort(_list, start, pivot_idx - 1)
        quicksort(_list, pivot_idx + 1, end)


if __name__ == '__main__':
    test_cases = [
        [11, 9, 29, 7, 2, 15, 28],
        [3, 7, 9, 11],
        [25, 22, 21, 10],
        [29, 15, 28],
        [],
        [6],
        [2, 1],
        [2, 1, 2]
    ]

    for test_case in test_cases:
        quicksort(test_case, 0, len(test_case) - 1)
        print(f'Sorted array: {test_case}')
